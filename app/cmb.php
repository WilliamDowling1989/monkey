<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/CMB2/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/cmb2/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/CMB2/init.php';
}

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 $cmb CMB2 object.
 *
 * @return bool      True if metabox should show
 */
function yourprefix_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's not the front page template.
	if ( get_option( 'page_on_front' ) !== $cmb->object_id ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field $field Field object.
 *
 * @return bool              True if metabox should show
 */
function yourprefix_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category.
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/**
 * Manually render a field.
 *
 * @param  array      $field_args Array of field arguments.
 * @param  CMB2_Field $field      The field object.
 */
function yourprefix_render_row_cb( $field_args, $field ) {
	$classes     = $field->row_classes();
	$id          = $field->args( 'id' );
	$label       = $field->args( 'name' );
	$name        = $field->args( '_name' );
	$value       = $field->escaped_value();
	$description = $field->args( 'description' );
	?>
	<div class="custom-field-row <?php echo esc_attr( $classes ); ?>">
		<p><label for="<?php echo esc_attr( $id ); ?>"><?php echo esc_html( $label ); ?></label></p>
		<p><input id="<?php echo esc_attr( $id ); ?>" type="text" name="<?php echo esc_attr( $name ); ?>" value="<?php echo $value; ?>"/></p>
		<p class="description"><?php echo esc_html( $description ); ?></p>
	</div>
	<?php
}

/**
 * Manually render a field column display.
 *
 * @param  array      $field_args Array of field arguments.
 * @param  CMB2_Field $field      The field object.
 */
function yourprefix_display_text_small_column( $field_args, $field ) {
	?>
	<div class="custom-column-display <?php echo esc_attr( $field->row_classes() ); ?>">
		<p><?php echo $field->escaped_value(); ?></p>
		<p class="description"><?php echo esc_html( $field->args( 'description' ) ); ?></p>
	</div>
	<?php
}

/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array      $field_args Array of field parameters.
 * @param  CMB2_Field $field      Field object.
 */
function yourprefix_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}

/**
 * Callback to define the optionss-saved message.
 *
 * @param CMB2  $cmb The CMB2 object.
 * @param array $args {
 *     An array of message arguments
 *
 *     @type bool   $is_options_page Whether current page is this options page.
 *     @type bool   $should_notify   Whether options were saved and we should be notified.
 *     @type bool   $is_updated      Whether options were updated with save (or stayed the same).
 *     @type string $setting         For add_settings_error(), Slug title of the setting to which
 *                                   this error applies.
 *     @type string $code            For add_settings_error(), Slug-name to identify the error.
 *                                   Used as part of 'id' attribute in HTML output.
 *     @type string $message         For add_settings_error(), The formatted message text to display
 *                                   to the user (will be shown inside styled `<div>` and `<p>` tags).
 *                                   Will be 'Settings updated.' if $is_updated is true, else 'Nothing to update.'
 *     @type string $type            For add_settings_error(), Message type, controls HTML class.
 *                                   Accepts 'error', 'updated', '', 'notice-warning', etc.
 *                                   Will be 'updated' if $is_updated is true, else 'notice-warning'.
 * }
 */
function yourprefix_options_page_message_callback( $cmb, $args ) {
	if ( ! empty( $args['should_notify'] ) ) {

		if ( $args['is_updated'] ) {

			// Modify the updated message.
			$args['message'] = sprintf( esc_html__( '%s &mdash; Updated!', 'cmb2' ), $cmb->prop( 'title' ) );
		}

		add_settings_error( $args['setting'], $args['code'], $args['message'], $args['type'] );
	}
}

/**
 * Only show this box in the CMB2 REST API if the user is logged in.
 *
 * @param  bool                 $is_allowed     Whether this box and its fields are allowed to be viewed.
 * @param  CMB2_REST_Controller $cmb_controller The controller object.
 *                                              CMB2 object available via `$cmb_controller->rest_box->cmb`.
 *
 * @return bool                 Whether this box and its fields are allowed to be viewed.
 */
function yourprefix_limit_rest_view_to_logged_in_users( $is_allowed, $cmb_controller ) {
	if ( ! is_user_logged_in() ) {
		$is_allowed = false;
	}

	return $is_allowed;
}




add_action( 'cmb2_admin_init', 'pageHeader' );
function pageHeader() {
	$prefix = 'page_header_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$pageHeader = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Page Header Options', 'cmb2' ),
		'object_types'  => array( 'page' ),
	) );

	$pageHeader->add_field( array(
		'name'       => esc_html__( 'Title', 'cmb2' ),
		'id'         => $prefix . 'title',
		'type'       => 'text',
	) );

	$pageHeader->add_field( array(
		'name'       => esc_html__( 'Intro', 'cmb2' ),
		'id'         => $prefix . 'intro',
		'type'       => 'textarea',
	) );

}




add_action( 'cmb2_admin_init', 'companiesBoxes' );
function companiesBoxes() {
	$prefix = 'companies_boxes_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$companyBoxes = new_cmb2_box( array(
		'id'            => $prefix . 'metabox',
		'title'         => esc_html__( 'Pick Location', 'cmb2' ),
		'object_types'  => array( 'companies' ),
	) );

	$companyBoxes->add_field( array(
		'name'       => esc_html__( 'Select location of this company', 'cmb2' ),
		'id'         => $prefix . 'location',
		'type'       => 'select',
		'options_cb' => 'cmb2_getLocations',
	) );

}






add_action( 'cmb2_admin_init', 'logoRepeater' );
function logoRepeater() {
	$prefix = 'logo_repeater_';

	$logorepeater = new_cmb2_box( array(
		'id'            => $prefix . 'logo_repeater',
		'title'         => esc_html__( 'Logos', 'cmb2' ),
		'object_types'  => array( 'page' ),
		'show_on'		=> array('key' => 'page-template', 'value' => array('views/template-home.blade.php', 'views/template-partner-with-us.blade.php'))
	) );

	$entry = $logorepeater->add_field( array(
	    'id'          => 'logo_repeater',
	    'type'        => 'group',
	    'options'     => array(
	        'group_title'   => __( 'Logo {#}', 'cmb2' ), 
	        'add_button'    => __( 'Add Another Logo', 'cmb2' ),
	        'remove_button' => __( 'Remove Logo', 'cmb2' ),
	        'sortable'      => true,
	    ),
	) );

		$logorepeater->add_group_field( $entry, array(
		    'name' => 'Image',
		    'id'   => 'image',
		    'type' => 'file',
		) );

}





add_action( 'cmb2_admin_init', 'aboutStorageMonkeyCTA' );
function aboutStorageMonkeyCTA() {
	$prefix = 'about_storage_monkey_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$aboutStorage = new_cmb2_box( array(
		'id'            => $prefix . 'about_storage',
		'title'         => esc_html__( 'About Storage Monkey', 'cmb2' ),
		'object_types'  => array( 'page' ),
		'show_on'		=> array('key' => 'page-template', 'value' => array('views/template-home.blade.php'))
	) );

	$aboutStorage->add_field( array(
		'name'       => esc_html__( 'Image', 'cmb2' ),
		'id'         => $prefix . 'image',
		'type'       => 'file',
	) );

	$aboutStorage->add_field( array(
		'name'       => esc_html__( 'Content', 'cmb2' ),
		'id'         => $prefix . 'content',
		'type'       => 'wysiwyg',
	) );

}



add_action( 'cmb2_admin_init', 'advertisingSpace' );
function advertisingSpace() {
	$prefix = 'advertising_space_';

	$aboutStorage = new_cmb2_box( array(
		'id'            => $prefix . 'advertising_space_',
		'title'         => esc_html__( 'Advertising Space', 'cmb2' ),
		'object_types'  => array( 'page' ),
		'show_on'		=> array('key' => 'page-template', 'value' => array('views/template-home.blade.php', 'views/template-blog.blade.php'))
	) );

	$aboutStorage->add_field( array(
		'name'       => esc_html__( 'Link', 'cmb2' ),
		'id'         => $prefix . 'link',
		'type'       => 'text',
	) );

	$aboutStorage->add_field( array(
		'name'       => esc_html__( 'Desktop Image', 'cmb2' ),
		'desc' => '<br>On home page the image can be as wide as possible (it will fit into the width of the website) but please ensure it is at least 400px to ensure good quality on mobile.

			<br><br>

			On the blog page please make sure the image is 368 × 471 pixels or as close to that as possible

			<br><br>',
		'id'         => $prefix . 'image',
		'type'       => 'file',
	) );


	$aboutStorage->add_field( array(
		'name'       => esc_html__( 'Mobile Image', 'cmb2' ),
		'desc' => 'Please make sure image is 500x500 or a 1x1 aspect ratio.',
		'id'         => $prefix . 'mobile_image',
		'type'       => 'file',
	) );

}






add_action( 'cmb2_admin_init', 'singleCompanies' );
function singleCompanies() {
	$prefix = 'single_companies_';

	$singleLocation = new_cmb2_box( array(
		'id'            => $prefix . 'single_companies_',
		'title'         => esc_html__( 'Company Options', 'cmb2' ),
		'object_types'  => array( 'companies' ),
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Sponsored Post?', 'cmb2' ),
		'id'         => $prefix . 'sponsored_post',
		'type'             => 'select',
		'options'          => array(
			'no' => __( 'No', 'cmb2' ),
			'yes'   => __( 'Yes', 'cmb2' ),
		),
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Company Logo', 'cmb2' ),
		'id'         => $prefix . 'company_logo',
		'type'       => 'file',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Prices From?', 'cmb2' ),
		'id'         => $prefix . 'prices_from',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Address', 'cmb2' ),
		'id'         => $prefix . 'address',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Directions Link', 'cmb2' ),
		'id'         => $prefix . 'direction_link',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Reviews By?', 'cmb2' ),
		'id'         => $prefix . 'reviews_by?',
		'type'       => 'select',
		'show_option_none' => true,
		'options'          => array(
			'feefo' => __( 'Feefo', 'cmb2' ),
			'trustpilot'   => __( 'Trustpilot', 'cmb2' ),
		),
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Review Text', 'cmb2' ),
		'id'         => $prefix . 'review_text',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Paragraph', 'cmb2' ),
		'id'         => $prefix . 'paragraph',
		'type'       => 'textarea',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Small Unit Price?', 'cmb2' ),
		'id'         => $prefix . 'small_unit_price',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Does this have a special offer?', 'cmb2' ),
		'id'         => $prefix . 'special_offer',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Website Link', 'cmb2' ),
		'id'         => $prefix . 'website_link',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Get A Quote Link', 'cmb2' ),
		'id'         => $prefix . 'get_a_quote_link',
		'desc' => 'If the link is an email link please add mailto: at the start of the email (for example - mailto:hello@storagemonkey.co.uk). If it is just a website link just paste the link in as normal.',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Phone Number', 'cmb2' ),
		'id'         => $prefix . 'phone_number',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Special Offer Link', 'cmb2' ),
		'id'         => $prefix . 'special_offer_link',
		'type'       => 'text',
	) );

	$singleLocation->add_field( array(
		'name'       => esc_html__( 'Slider Section', 'cmb2' ),
		'id'         => $prefix . 'slider_title',
		'type'       => 'title',
	) );

	$entry = $singleLocation->add_field( array(
	    'id'          => 'slider_repeater',
	    'type'        => 'group',
	    'options'     => array(
	        'group_title'   => __( 'Slider {#}', 'cmb2' ), 
	        'add_button'    => __( 'Add Another Slide', 'cmb2' ),
	        'remove_button' => __( 'Remove Slide', 'cmb2' ),
	        'sortable'      => true,
	    ),
	) );

	$singleLocation->add_group_field( $entry, array(
	    'name' => 'Image',
	    'id'   => 'image',
	    'type' => 'file',
	) );

	$singleLocation->add_group_field( $entry, array(
	    'name' => 'Video Link',
	    'desc' => 'Video number for YouTube (e.g S_HmQ4ifoZ4)',
	    'id'   => 'text',
	    'type' => 'text',
	) );

}





add_action( 'cmb2_admin_init', 'locationRepeater' );
function locationRepeater() {
	$prefix = 'location_repeater_';

	$logorepeater = new_cmb2_box( array(
		'id'            => $prefix . 'location_repeater',
		'title'         => esc_html__( 'Top Locations', 'cmb2' ),
		'object_types'  => array( 'page' ),
		'show_on'		=> array('key' => 'page-template', 'value' => array( 'views/template-all-locations.blade.php'))
	) );

	$entry = $logorepeater->add_field( array(
	    'id'          => 'locations_repeater',
	    'type'        => 'group',
	    'options'     => array(
	        'group_title'   => __( 'Location {#}', 'cmb2' ), 
	        'add_button'    => __( 'Add Another Location', 'cmb2' ),
	        'remove_button' => __( 'Remove Location', 'cmb2' ),
	        'sortable'      => true,
	    ),
	) );

		$logorepeater->add_group_field( $entry, array(
		    'name' => 'Image',
		    'id'   => 'image',
		    'type' => 'file',
		) );

		$logorepeater->add_group_field( $entry, array(
		    'name' => 'Name',
		    'id'   => 'name',
		    'type' => 'text',
		) );

		$logorepeater->add_group_field( $entry, array(
		    'name' => 'Link',
		    'id'   => 'link',
		    'type' => 'text',
		) );

}
