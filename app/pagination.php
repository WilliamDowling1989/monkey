<?php

if ( !function_exists('DiPagination')) 
{

    function DiPagination($custom_query = null) 
    {
        if( ! $custom_query )
        {
            global $wp_query;
            $custom_query = $wp_query;
        }
        
        if ( !$current_page = get_query_var( 'paged' ) )
        {
            $current_page = 1;
        }

        $permalinks = get_option( 'permalink_structure' );

        if( is_front_page() )
        {
            $format = empty( $permalinks ) ? '?paged=%#%' : 'page/%#%/';
        } 
        else 
        {
            $format = empty( $permalinks ) || is_search() ? '&paged=%#%' : 'page/%#%/';
        }

        $big = 999999999; // need an unlikely integer

        $pagination = paginate_links( 
            array(
                'base' => str_replace( $big, '%#%', get_pagenum_link( $big, false ) ),
                'format' => $format,
                'current' => $current_page,
                'total' => $custom_query->max_num_pages,
                'mid_size' => '4',
                'type' => 'list',
                'next_text' => __( 'Next' ),
                'prev_text' => __( 'Previous' )
            ) 
        );

        $pagination = explode( "\n", $pagination );
        $pagination_mod = array();

        foreach ( $pagination as $item )
        {
            ( preg_match( '/<ul class=\'page-numbers\'>/i', $item ) ) ? $item = str_replace( '<ul class=\'page-numbers\'>', '<ul class=\'pagination\'>', $item ) : $item;
            $item = str_replace( '<li', '<li class="page-item"', $item );
            // ( preg_match( '/class="next/i', $item ) ) ? $item = str_replace( '<li', '<li class="page-item"', $item ) : $item;
            ( preg_match( '/page-numbers/i', $item ) ) ? $item = str_replace( 'page-numbers', 'page-numbers page-link', $item ) : $item;
            $pagination_mod[] .= $item;
        }

?>

        <div class="pagination-wrapper">

            <?php 
                foreach( $pagination_mod as $page )
                {
                    echo $page;
                }
            ?>
        </div>
<?php 
    }
}