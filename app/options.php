<?php
/**
 * CMB2 Theme Options
 * @version 0.1.0
 */
class DiOptions{

    /**
     * Option key, and option page slug
     * @var string
     */
    private $key = 'di_options';

    /**
     * Options page metabox id
     * @var string
     */
    private $metabox_id = 'di_option_metabox';

    /**
     * Options Page title
     * @var string
     */
    protected $title = '';

    /**
     * Options Page hook
     * @var string
     */
    protected $options_page = '';

    /**
     * Holds an instance of the object
     *
     * @var DiOptions
     **/
    private static $instance = null;

    /**
     * Constructor
     * @since 0.1.0
     */
    private function __construct() {
        // Set our title
        $this->title = __( 'Site Options', 'di' );
    }

    /**
     * Returns the running object
     *
     * @return DiOptions
     **/
    public static function get_instance() {
        if( is_null( self::$instance ) ) {
            self::$instance = new DiOptions();
            self::$instance->hooks();
        }
        return self::$instance;
    }

    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
        add_action( 'admin_init', array( $this, 'init' ) );
        add_action( 'admin_menu', array( $this, 'add_options_page' ) );
        add_action( 'cmb2_admin_init', array( $this, 'add_options_page_metabox' ) );
    }


    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {
        register_setting( $this->key, $this->key );
    }

    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        $this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );

        // Include CMB CSS in the head to avoid FOUC
        add_action( "admin_print_styles-{$this->options_page}", array( 'CMB2_hookup', 'enqueue_cmb_css' ) );
    }

    /**
     * Admin page markup. Mostly handled by CMB2
     * @since  0.1.0
     */
    public function admin_page_display() {
        ?>
        <div class="wrap cmb2-options-page <?php echo $this->key; ?>">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
            <?php cmb2_metabox_form( $this->metabox_id, $this->key ); ?>
        </div>
        <?php
    }

    /**
     * Add the options metabox to the array of metaboxes
     * @since  0.1.0
     */
    function add_options_page_metabox() {

        // hook in our save notices
        add_action( "cmb2_save_options-page_fields_{$this->metabox_id}", array( $this, 'settings_notices' ), 10, 2 );

        $cmb = new_cmb2_box( array(
            'id'         => $this->metabox_id,
            'hookup'     => false,
            'cmb_styles' => false,
            'show_on'    => array(
                // These are important, don't remove
                'key'   => 'options-page',
                'value' => array( $this->key, )
            ),
        ) );

        // Set our CMB2 fields

        $cmb->add_field( array(
            'name' => __( 'Phone Number', 'di' ),
            'id'   => 'site_phone',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Partner Enquries', 'di' ),
            'id'   => 'partner_email',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => __( 'General Enquries', 'di' ),
            'id'   => 'general_email',
            'type' => 'text'
        ) );


        $cmb->add_field( array(
            'name' => __( '<h1>Blog Ad Space</h1>', 'di' ),
            'type' => 'title',
            'id'   => 'title'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Blog Ad Sidebar Image', 'di' ),
            'id'   => 'site_blog_ad_image',
            'type' => 'file'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Blog Ad Sidebar Link', 'di' ),
            'id'   => 'site_blog_ad_link',
            'type' => 'text'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Blog Ad Bottom Image', 'di' ),
            'id'   => 'site_blog_ad_bottom_image',
            'desc' => 'Try and make image 774x295 or as close to that size as possible',
            'type' => 'file'
        ) );

        $cmb->add_field( array(
            'name' => __( 'Blog Ad Bottom Link', 'di' ),
            'id'   => 'site_blog_ad_bottom_link',
            'type' => 'text'
        ) );

          $cmb->add_field( array(
            'name' => __( '<h1>Social Links</h1>', 'di' ),
            'type' => 'title',
            'desc' => 'These links will only show in footer if these boxes are filled in with the relevant links.',
            'id'   => 'social_title'
        ) );

          $cmb->add_field( array(
            'name' => __( 'Facebook', 'di' ),
            'id'   => 'facebook',
            'type' => 'text'
        ) );

          $cmb->add_field( array(
            'name' => __( 'Twitter', 'di' ),
            'id'   => 'twitter',
            'type' => 'text'
        ) );

          $cmb->add_field( array(
            'name' => __( 'LinkedIn', 'di' ),
            'id'   => 'linkedin',
            'type' => 'text'
        ) );

          $cmb->add_field( array(
            'name' => __( 'Google', 'di' ),
            'id'   => 'google',
            'type' => 'text'
        ) );

          $cmb->add_field( array(
            'name' => __( '<h1>Tooltip Text</h1>', 'di' ),
            'type' => 'title',
            'id'   => 'tooltip_title'
        ) );

          $cmb->add_field( array(
            'name' => __( 'Text', 'di' ),
            'id'   => 'tooltip_text',
            'type' => 'textarea'
        ) );

    }

    /**
     * Register settings notices for display
     *
     * @since  0.1.0
     * @param  int   $object_id Option key
     * @param  array $updated   Array of updated fields
     * @return void
     */
    public function settings_notices( $object_id, $updated ) {
        if ( $object_id !== $this->key || empty( $updated ) ) {
            return;
        }

        add_settings_error( $this->key . '-notices', '', __( 'Settings updated.', 'di' ), 'updated' );
        settings_errors( $this->key . '-notices' );
    }

    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {
        // Allowed fields to retrieve
        if ( in_array( $field, array( 'key', 'metabox_id', 'title', 'options_page' ), true ) ) {
            return $this->{$field};
        }

        throw new Exception( 'Invalid property: ' . $field );
    }

}

/**
 * Helper function to get/return the DiOptions object
 * @since  0.1.0
 * @return DiOptions object
 */
function di_options() {
    return DiOptions::get_instance();
}

/**
 * Wrapper function around cmb2_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function di_get_option( $key = '' ) {
    return cmb2_get_option( di_options()->key, $key );
}

// Get it started
di_options();