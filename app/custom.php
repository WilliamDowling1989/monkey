<?php

/**
 * Return site email
 * 
 * @return string
 */
function getSitePhone()
{
    return di_get_option('site_phone');
}

/**
 * Return partner enquries
 * 
 * @return string
 */
function getPartnerEmail()
{
    return di_get_option('partner_email');
}


/**
 * Return partner enquries
 * 
 * @return string
 */
function getGeneralEmail()
{
    return di_get_option('general_email');
}

/**
 * Return blog ad image
 * 
 * @return string
 */
function getBlogAdImage()
{
    return di_get_option('site_blog_ad_image');
}

/**
 * Return site blog ad link
 * 
 * @return string
 */
function getBlogAdLink()
{
    return di_get_option('site_blog_ad_link');
}


/**
 * Return blog bottom ad image
 * 
 * @return string
 */
function getBlogAdBottomImage()
{
    return di_get_option('site_blog_ad_bottom_image');
}

/**
 * Return site blog ad link
 * 
 * @return string
 */
function getBlogAdBottomLink()
{
    return di_get_option('site_blog_ad_bottom_link');
}

/**
 * Return facebook
 * 
 * @return string
 */
function getSiteFacebook()
{
    return di_get_option('facebook');
}

/**
 * Return twitter
 * 
 * @return string
 */
function getSiteTwitter()
{
    return di_get_option('twitter');
}

/**
 * Return twitter
 * 
 * @return string
 */
function getSiteLinkedin()
{
    return di_get_option('linkedin');
}

/**
 * Return twitter
 * 
 * @return string
 */
function getSiteGoogle()
{
    return di_get_option('google');
}

/**
 * Return site tooltip
 * 
 * @return string
 */
function getSiteTooltipText()
{
    return di_get_option('tooltip_text');
}


// Stop wysyiwg editor removing span tags
function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
} add_filter('tiny_mce_before_init', 'override_mce_options');


// Get all locations
function cmb2_getLocations( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type'   => 'location',
        'orderby'   => 'title',
        'order' => 'DESC',
        'posts_per_page' => -1,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }

    return $post_options;
}




// Get all companies
function cmb2_getCompanies( $query_args ) {

    $args = wp_parse_args( $query_args, array(
         'post_type'     => 'companies',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'meta_key' => 'companies_boxes_location',
        'meta_value' => get_the_ID()
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
          $post_options[ $post->ID ] = $post->post_title;
        }
    }

    return $post_options;
}




// Get current location
function getCurrentLocation(){

    return new WP_Query([
        'post_type'     => 'companies',
        'posts_per_page' => -1,
        'order' => 'ASC',
       'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'companies_boxes_location',
                'value'   => get_the_ID(),
            ),
            array(
                'key' => 'single_companies_sponsored_post',
                'value'   => 'no',
            ),
        ),
    ]);

}





// Get sponsored company
function getSponsoredCompany(){

    return new WP_Query([
        'post_type'     => 'companies',
        'posts_per_page' => -1,
        'order' => 'DESC',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'companies_boxes_location',
                'value'   => get_the_ID(),
            ),
            array(
                'key' => 'single_companies_sponsored_post',
                'value'   => 'yes',
            ),
        ),
    ]);

}





// Allow SVGs to be uploaded
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



/**
 * Returns all locations
 * @return WP_Query
 */
function getLocations($taxonomy)
{
    return new WP_Query(array(
        'post_type' => 'location',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'region',
                'field'    => 'slug',
                'terms'    => $taxonomy,
            ),
        ),
    ));
}



/**
 * Loop to get blog posts
 * 
 * @return array
 */
function getBlogPosts($perPage = 8){

    $paged = get_query_var('paged') ? get_query_var('paged') : 1;

    $args = [
        'post_type' => 'post',
        'order' => 'DESC',
        'posts_per_page' => $perPage,
        'paged' => $paged,
    ];

    return new WP_Query($args);
}




function getQuoteSizes(){
    

    return [

        1 => [
            'text' => 'Car (10-25 sq ft)',
            'image' => App\asset_path('images/1.svg'),
        ],

        2 => [
            'text' => 'Small Van (20-35 sq ft)',
            'image' => App\asset_path('images/2.svg'),
        ],

        3 => [
            'text' => 'Transit Van (35-50 sq ft)',
            'image' => App\asset_path('images/3.svg'),
        ],

        4 => [
            'text' => 'Medium Luton Van (50-75 sq ft)',
            'image' => App\asset_path('images/4.svg'),
        ],

        5 => [
            'text' => 'Large Luton Van (75-100 sq ft) ',
            'image' => App\asset_path('images/5.svg'),
        ],

        6 => [
            'text' => 'Removal Box Van 100-150 sq ft)',
            'image' => App\asset_path('images/6.svg'),
        ],

        7 => [
            'text' => 'Removal Truck (150-250 sq ft)',
            'image' => App\asset_path('images/7.svg'),
        ],

        8 => [
            'text' => 'Other (250 sq ft +)',
            'image' => App\asset_path('images/8.svg'),
        ],

    ];
}




add_action( 'gform_save_field_value_2_2', 'pre_submission_handler' );
function pre_submission_handler( $value ) {

    $sizesArray = getQuoteSizes();

    if( ! array_key_exists($value, $sizesArray) ) return 'Size not found';
    
    return $sizesArray[$value]['text'];

}




