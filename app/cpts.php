<?php

class DiPostTypes {

    /**
     * The post types we want to create
     * 
     * @var array
     */
    protected $types = [
        [
            'name'      => 'location',
            'singular'  => 'Location',
            'plural'    => 'Locations',
            'supports'  => ['title', 'editor', 'thumbnail'],
            'icon'   => 'dashicons-admin-post',
            'public'    => true,
            'taxonomy'  => array(
                [
                    'name'      => 'region',
                    'singular'  => 'Region',
                    'plural'    => 'Regions'
                ],
            ),
        ],
        [
            'name'      => 'companies',
            'singular'  => 'Company',
            'plural'    => 'Companies',
            'icon'   => 'dashicons-admin-multisite',
            'supports'  => ['title', 'editor', 'thumbnail'],
            'public'    => true,
            'taxonomy'  => array(
                [
                    'name'      => 'locations',
                    'singular'  => 'Location',
                    'plural'    => 'Locations'
                ],
            ),
        ]
    ];

    /**
     * Constructor method
     */
    public function __construct()
    {
        add_action('init', [$this, 'createTaxonomies'], 0);
        add_action('init', [$this, 'createPostTypes']);
    }

    /**
     * Create the post types
     * 
     * @return void
     */
    public function createPostTypes()
    {
        foreach( $this->types as $type )
        {
            if( isset($type['public']) && $type['public'] == false )
            {
                $args = [
                    'exclude_from_search' => true,
                    'publicly_queryable' => false,
                    'show_ui' => true,
                    'show_in_menu' => true,
                ];
            }
            else
            {
                $args = ['public' => true];
            }

            if( isset($type['icon'])  )
            {
                $args['menu_icon'] = $type['icon'];
            }

            register_post_type( $type['name'],
                array(
                    'labels' => array(
                        'name' => __( $type['plural'] ),
                        'singular_name' => __( $type['singular'] ),
                        'add_new' => __( 'Add New' ),
                        'add_new_item' => __( 'Add New ' . $type['singular'] ),
                        'edit' => __( 'Edit' ),
                        'edit_item' => __( 'Edit ' . $type['singular'] ),
                        'new_item' => __( 'New ' . $type['singular'] ),
                        'view' => __( 'View' ),
                        'view_item' => __( 'View ' . $type['singular'] ),
                        'search_items' => __( 'Search ' . $type['plural'] ),
                        'not_found' => __( 'No ' . $type['plural'] . ' found' ),
                        'not_found_in_trash' => __( 'No ' . $type['plural'] . ' found in Trash' ),
                        'parent' => __( 'Parent ' . $type['singular'] )
                        ),
                    'supports' => $type['supports'],
                    'has_archive' =>  isset($type['archive']) && $type['archive'] ? $type['archive'] : false,
                    'show_in_rest' => true,
                    'hierarchical' => true,
                    'rewrite' => ['with_front' => false]
                ) + $args
            );
        }
    }

    /**
     * Create any specified taxonomies for the custom post type
     * 
     * @return void
     */
    public function createTaxonomies()
    {
        foreach( $this->types as $type )
        {
            if( isset($type['taxonomy']) )
            {
                $taxonomy = $type['taxonomy'];

                foreach( $taxonomy as $tax )
                {
                    $plural     = $tax['plural'];
                    $singular   = $tax['singular'];

                    $labels = array(
                        'name'              => _x( $plural, 'taxonomy general ' . $singular ),
                        'singular_name'     => _x( $singular, 'taxonomy singular ' . $singular ),
                        'search_items'      =>  __( 'Search ' . $singular ),
                        'all_items'         => __( 'All ' . $singular ),
                        'parent_item'       => __( 'Parent ' . $singular ),
                        'parent_item_colon' => __( 'Parent : . $singular' ),
                        'edit_item'         => __( 'Edit ' . $singular ),
                        'update_item'       => __( 'Update ' . $singular ),
                        'add_new_item'      => __( 'Add New ' . $singular ),
                        'new_item_name'     => __( 'New ' . $singular ),
                    );  

                    register_taxonomy( $tax['name'], array($type['name']), array(
                        'hierarchical'  => true,
                        'labels'        => $labels,
                        'show_ui'       => true,
                        'query_var'     => true,
                        'show_in_rest'  => true
                    ));
                }
            }
        }
    }

}

new DiPostTypes;