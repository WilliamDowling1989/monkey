<header class="banner">

	<div class="container">

		<div class="banner__inner">

			<a class="brand" href="{{ home_url('/') }}">
				<img src="@asset('images/header-logo.svg')" alt="Storage Monkey">
			</a>

			<button class="menu-toggle"><i class="fas fa-bars"></i></button>

			<nav class="nav-primary">
				@if (has_nav_menu('primary_navigation'))
					{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
				@endif
			</nav>

		</div>

	</div>

</header>
