<div class="single-post">
  
	<div class="container">

		<div class="single-post__inner">

			<div class="share-buttons">
		  		<img src="@asset('images/share-icon.svg')" alt="Share">

		  		<p>
		  			<span>S</span>
		  			<span>h</span>
		  			<span>a</span>
		  			<span>r</span>
		  			<span>e</span>
		  		</p>

		  		<a href="https://twitter.com/home?status=@php(the_permalink())" target="_blank">
		  			<img src="@asset('images/twitter-icon.svg')" alt="Twitter">
		  		</a>

		  		<a href="https://www.facebook.com/sharer/sharer.php?u=@php(the_permalink())" target="_blank">
		  			<img src="@asset('images/facebook-icon.svg')" alt="Facebook">
		  		</a>

		  		<a href="https://www.linkedin.com/shareArticle?mini=true&url=@php(the_permalink())&title=@php(the_title())&summary=&source=" target="_blank">
		  			<img src="@asset('images/linkedin-icon.svg')" alt="LinkedIn">
				</a>

				<a href="https://plus.google.com/share?url=@php(the_permalink())" target="_blank">
					<img src="@asset('images/google-icon.svg')" alt="Google">
		  		</a>

		  	</div>
	
			<div class="content">

				<p class="categories"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . '<span>, </span>'; } ?></p>
			
				<h1>@php(the_title())</h1>

				<span class="date"><?php echo get_the_date('dS M Y'); ?></span>

				@php(the_content())

				<a href="{!! getBlogAdBottomLink() !!}"><img class="alignnone size-full wp-image-91" src="{!! getBlogAdBottomImage() !!}" alt="" width="774" height="295" /></a>

			</div>

			<div class="banner-section">
				
				<div class="banner-space banner-space__blog">
					<a href="{!! getBlogAdLink() !!}">
						<img src="{!! getBlogAdImage() !!}" alt="">
					</a>	
				</div>

			</div>

		</div>

	</div>

</div>

@include('partials.latest-blog-posts')