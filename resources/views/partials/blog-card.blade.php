@php($post)
@php($image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ))

<div id="post-<?php the_ID(); ?>" class="blog-card--v2">

	<div class="overlay"></div>	

	<a href="{!! $di_link; !!}"><div class="image" style="background-image: url(<?php echo $image[0]; ?> ) !important;"></div></a>

	<div class="body">

		<span class="category"><?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?></span>

		<h3><a href="{!! $di_link; !!}">{!! mb_strimwidth($di_title, 0,  48, '...'); !!}</a></h3>

		<p>
			<?php
				$content = get_the_content();
				echo wp_trim_words( $content , '20' ); 
			?>...

		</p>

		<div class="date">
			<i class="far fa-calendar"></i> {!! get_the_date( 'j F Y' ); !!}
		</div>
		
	</div>
				
</div>