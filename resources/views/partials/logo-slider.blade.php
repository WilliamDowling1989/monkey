<div class="logo-slider">
	
	<div class="container">
		
		<div class="logo-slider__inner">
			
				<?php
					$entries = get_post_meta( get_the_ID(), 'logo_repeater', true );

					foreach ( (array) $entries as $key => $entry ) :

					$image  = '';

					if ( isset( $entry['image'] ) )
					$image = esc_html( $entry['image'] );
									
				?>
					<div class="item">
						<img src="{!! $image !!}" alt="">
					</div>
								
				<?php
					endforeach;	
				?>

		</div>

	</div>

</div>