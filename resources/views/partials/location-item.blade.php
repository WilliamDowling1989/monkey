@php($loop = getCurrentLocation())

			@if( $loop->have_posts() )
				@php($count=1)
				  @while( $loop->have_posts() )
				    @php( $loop->the_post() )
					
					@php($logo = get_post_meta( get_the_ID(), 'single_companies_company_logo', true ))
					@php($checkbox = get_post_meta( get_the_ID(), 'single_companies_sponsored_post', true ))
				    @php($monthlyPrice = get_post_meta( get_the_ID(), 'single_companies_prices_from', true ))
				    @php($address = get_post_meta( get_the_ID(), 'single_companies_address', true ))
				    @php($directionsLink = get_post_meta( get_the_ID(), 'single_companies_direction_link', true ))
					@php($reviewsBy = get_post_meta( get_the_ID(), 'single_companies_reviews_by?', true ))
					@php($reviewText = get_post_meta( get_the_ID(), 'single_companies_review_text', true ))
					@php($paragraph = get_post_meta( get_the_ID(), 'single_companies_paragraph', true ))
					@php($smallunitprice = get_post_meta( get_the_ID(), 'single_companies_small_unit_price', true ))
					@php($specialOffer = get_post_meta( get_the_ID(), 'single_companies_special_offer', true ))
					@php($websiteLink = get_post_meta( get_the_ID(), 'single_companies_website_link', true ))
					@php($getAQuote = get_post_meta( get_the_ID(), 'single_companies_get_a_quote_link', true ))
					@php($phoneNumber = get_post_meta( get_the_ID(), 'single_companies_phone_number', true ))
					@php($specialOfferLink = get_post_meta( get_the_ID(), 'single_companies_special_offer_link', true ))

				    	<div class="location-item">
				    		
				    		<div class="left">
							
							<div class="company-logo">
								<img src="{!! $logo !!}" alt="@php(the_title())">
							</div>
				    			
				    			<div class="slider">
				    				
			    					<?php

									$entries = get_post_meta( get_the_ID(), 'slider_repeater', true );

									foreach ( (array) $entries as $key => $entry ) :

									$image = $video = '';

									if ( isset( $entry['image'] ) )
									$image = esc_html( $entry['image'] );

									if ( isset( $entry['text'] ) )
									$video = esc_html( $entry['text'] );
														
								?>

									<div class="slide">

				    						<img src="{!! $image !!}" alt="@php(the_title()) Storage Companies">

				    						@if(! empty($video))

				    							<button class="play-button" data-video-id="{!! $video !!}"><img src="@asset('images/play-button.svg')" alt=""></button>
										
										@endif

				    					</div>

			    					<?php
									endforeach;	
								?>

				    			</div>

				    		</div>

				    		<div class="right" data-number="0{!! $count !!}">

				    			@if($count == 1)
								<span class="top-pick">Top Pick</span>
				    			@endif

							<h2>@php(the_title())</h2>

							@if ( $monthlyPrice == 'Call for best price' )
								
								<p class="monthly-price" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">{!! $monthlyPrice !!}</p>

							@else
								
								<p class="monthly-price" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">Monthly prices from £{!! $monthlyPrice !!} <span>per sq ft</span></p>

							@endif

							<p class="address">
								{!! $address !!} 
								<a href="{!! $directionsLink !!}">
									<i class="fas fa-map-marker"></i> Get Directions</a>
							</p>

							@if($reviewsBy == 'feefo')
								<div class="reviews" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">
									<img src="@asset('images/feefo.png')" alt="Feefo">
									<p>{!! $reviewText !!}</p>
								</div>
							@elseif($reviewsBy == 'trustpilot')
								<div class="reviews" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">
									<img src="@asset('images/trustpilot.png')" alt="Trustpilot">
									<p>{!! $reviewText !!}</p>
								</div>
							@elseif($reviewsBy == 'none')

							@endif

							<p class="information">{!! $paragraph !!}</p>

							@if ( $smallunitprice == 'Call for best price' )
								
								<p class="small-monthly-price" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">{!! $smallunitprice !!}</p>

							@else
								
								<p class="small-monthly-price" data-toggle="tooltip" data-placement="right" title="{!! getSiteTooltipText() !!}">Small Unit from £{!! $smallunitprice !!} PER MONTH</p>

							@endif

							<p class="special-offer"><i class="fas fa-star"></i> <a href="{!! $specialOfferLink !!}">Get Special Offer</a> - {!! $specialOffer !!}</p>

							<ul class="links">
								<li>
									<a href="{!! $websiteLink !!}" target="_blank">Visit Site</a>
								</li>
								<li>
									<a href="{!! $getAQuote !!}">Get A Quote</a>
								</li>
								<li>
									<a href="tel:{!! $phoneNumber !!}">Call</a>
								</li>
							</ul>

				    		</div>

				    		

				    	</div>
				  	@php($count++)
				  @endwhile
				@endif

				@php(wp_reset_postdata())
