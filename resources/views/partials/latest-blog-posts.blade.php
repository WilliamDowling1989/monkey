<div class="latest-blog-posts @if(is_single()) latest-blog-posts__background @endif ">

	<div class="container">

		<h3>Get The Latest Blog Posts & Guides</h3>
		
		@if(is_singular('post'))
			<p>Here at StorageMonkey we spend a lot of time pulling together useful information to guide you through the storage jungle.</p>
		@endif

		

	  	<div class="latest-blog-posts__inner">
	  		
	  		<?php global $post; ?>
	  		@php($loop = getBlogPosts())

			@if( $loop->have_posts() )
				@while( $loop->have_posts() )
					@php( $loop->the_post() )
					
					@include('partials.blog-card', [
					    'di_title' => get_the_title(),
					    'di_link'  => get_permalink(),
					     'di_category_name'  => get_the_category()
					])
				@endwhile
			@endif

			@php(wp_reset_postdata())

	  	</div>

	</div>

</div>