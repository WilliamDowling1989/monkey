@php($image = get_post_meta( get_the_ID(), 'about_storage_monkey_image', true ) )
@php($content = wpautop( get_post_meta( get_the_ID(), 'about_storage_monkey_content', true ) ) )

<div class="about-storage-monkey">
	
	<div class="container">
		
		<div class="about-storage-monkey__inner">
			
			<div class="image">
				<img src="{!! $image; !!}" alt="About Storage Monkey">
			</div>

			<div class="content">
				
				{!! $content; !!}

			</div>

		</div>

	</div>

</div>