<footer class="content-info">
	
	<div class="container">

		<img src="@asset('images/header-logo.svg')" alt="Storage Monkey">

		<ul class="social-links">
			
			@if ( getSiteFacebook() )
				<li>
					<a href="{!! getSiteFacebook() !!}"><i class="fab fa-facebook"></i></a>
				</li>
			@endif
			
			@if ( getSiteTwitter() )
				<li>
					<a href="{!! getSiteTwitter() !!}"><i class="fab fa-twitter"></i></a>
				</li>
			@endif

			@if ( getSiteLinkedin() )
				<li>
					<a href="{!! getSiteLinkedin() !!}"><i class="fab fa-linkedin"></i></a>
				</li>
			@endif

			@if ( getSiteGoogle() )
				<li>
					<a href="{!! getSiteGoogle() !!}"><i class="fab fa-google"></i></a>
				</li>
			@endif
			
		</ul>
		
		<nav class="nav-primary">
			@if (has_nav_menu('footer_navigation'))
				{!! wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav']) !!}
			@endif
		</nav>

	</div>

	<script src="{!! get_template_directory_uri() !!}/assets/scripts/modal-video.min.js"></script>

</footer>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106748163-1"></script>

<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-106748163-1');
</script>