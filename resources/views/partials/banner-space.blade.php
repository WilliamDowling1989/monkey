@php($link = get_post_meta( get_the_ID(), 'advertising_space_link', true ) )
@php($image = get_post_meta( get_the_ID(), 'advertising_space_image', true ) ) 
@php($mobileImage = get_post_meta( get_the_ID(), 'advertising_space_mobile_image', true ) ) 

<div class="banner-space">
	
	<div class="container">
		
		<div class="banner-space__inner">
			
			<a href="{!! $link !!}" class="desktop-image">
				<img src="{!! $image !!}" alt="">
			</a >

			<a href="{!! $link !!}" class="mobile-image">
				<img src="{!! $mobileImage !!}" alt="">
			</a >
			
		</div>

	</div>

</div>