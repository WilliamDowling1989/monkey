<div class="all-locations">

	<div class="container">

		@if(is_page('locations'))
			<h1>All Locations</h2>
		@else
			<h2>Review and Compare Storage In These Locations</h2>
		@endif	
		
		<div class="all-locations__inner">	
			
				<?php $categories = get_terms('region'); ?>

				<?php if( count($categories) ) : ?>

					<?php foreach( $categories as $category ) : ?>

						<div class="all-locations__inner--item">
						
							<h3><?= $category->name; ?></h2>

							<ul class="locations-list">
								
								<?php $locations = getLocations($category->slug); ?>

								<?php if( $locations->have_posts() ) : ?>

									<?php while( $locations->have_posts() ) : ?>

										<?php $locations->the_post(); ?>

											<li>
												<a href="@php(the_permalink())">@php(the_title())</a>
											</li>

									<?php endwhile; ?>

								<?php endif; ?>

								<?php wp_reset_postdata(); ?>

							</ul>

						</div>

					<?php endforeach; ?>

				<?php endif; ?>

		</div>

	</div>

</div>