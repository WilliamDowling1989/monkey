@php($title = get_post_meta( get_the_ID(), 'page_header_title', true ))
@php($intro = get_post_meta( get_the_ID(), 'page_header_intro', true ))

<div class="page-header">
	<div class="container">
		<h1>{!! $title !!}</h1>
		<p>{!! $intro !!}</p>
	</div>
</div>
