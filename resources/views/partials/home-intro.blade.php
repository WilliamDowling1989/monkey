@php($post)
@php($src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ))

<div class="home-intro" style="background-image: url(<?php echo $src[0]; ?> );">
	
	<div class="container">
		
		<div class="home-intro__inner">
			
			@php(the_content())

			<a href="{{ home_url('/') }}locations" class="button button__pink button__large">View All Locations</a>

		</div>

	</div>

</div>