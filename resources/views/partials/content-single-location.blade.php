@php($post)
@php($src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' ))

<div class="single-location-page">

	<div class="single-location-page__header" style="background-image: url(<?php echo $src[0]; ?> ) !important;">

		<div class="overlay"></div>
		
		<div class="container">
			
			<span>Top 5</span>
			
			<h1>@php(the_title()) Storage Companies</h1>

			<p>@php(the_content())</p>

		</div>

	</div>
			
	@include('partials.sponsored-location')
	
	<div class="container">
		
		<div class="single-location-page__inner">
			
			@include('partials.location-item')
			
		</div>

	</div>

</div>

@include('partials.latest-blog-posts')	