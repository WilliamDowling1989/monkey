<div class="storage-cta">
	
	<h2>Compare Hundreds Of Storage Providers Across The UK</h2>

	<a href="{{ home_url('/') }}/locations" class="button button__pink button__large">View All Locations</a>

</div>