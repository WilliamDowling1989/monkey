{{--
  Template Name: All Locations
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp
		
		<div class="all-locations-page">
			
			<div class="container">
				
				<div class="all-locations-page__inner">
					
					<div class="all-locations-page__inner--top">
						
						<?php
							$entries = get_post_meta( get_the_ID(), 'locations_repeater', true );

							$count=0;
							
							foreach ( (array) $entries as $key => $entry ) :

							$image = $name = $link = '';

							if ( isset( $entry['image'] ) )
								$image = esc_html( $entry['image'] );

							if ( isset( $entry['name'] ) )
								$name = esc_html( $entry['name'] );

							if ( isset( $entry['link'] ) )
								$link = esc_html( $entry['link'] );

						?>

							<a class="all-locations-page__inner--top--item" href="{{ home_url('/') }}{!! $link; !!}" style="background-image: url('{!! $image !!}');">
								<span class="overlay"></span>
								<h3>{!! $name !!}</h3>
							</a>										
										
						<?php
							$count++; endforeach;	
						?>

					</div>

				</div>

			</div>

			@include('partials.all-locations')

		</div>

	@endwhile

@endsection
