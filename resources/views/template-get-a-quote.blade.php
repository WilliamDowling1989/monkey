{{--
  Template Name: Get A Quote
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp

		<?php $sizes = getQuoteSizes(); ?>
		<div data-size-illustrations='<?php echo json_encode($sizes); ?>'></div>

		<div class="get-a-quote-page">

			<div class="container">
			
				<div class="get-a-quote-page__header">
					
					<h1>Get Storage Quotes</h1>

					<p>We can help you find great storage at unbelievable rates. Simply tell us what you need and we'll send you up to 3 quotes from our list of preferred storage partners in your local area. StorageMonkey has access to exclusive rates and a range of special offers and discounts for students, domestic customers and businesses.</p>

				</div>

				<div class="get-a-quote-page__content">

					{!! do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]') !!}

				</div>

			</div>

		</div>

	@endwhile

@endsection
