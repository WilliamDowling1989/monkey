{{--
  Template Name: Standard Page
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp
		
		<div class="standard">
			
			<div class="container">
					
				<div class="standard__inner">
					
					<h1>@php(the_title())</h1>

					@php(the_content())

					@include('partials.storage-cta')

				</div>

			</div>

		</div>

	@endwhile

@endsection
