{{--
  Template Name: Partner With Us
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp

		@include('partials.page-header')

		@include('partials.logo-slider')

		<div class="partner-with-us-page">
			
			<div class="container">
				
				<div class="partner-with-us-page__inner">
					
					<div class="partner-with-us-page__inner--form">
						
						{!! do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]') !!}

					</div>
					
					<div class="partner-with-us-page__inner--sidebar">
						
						<h3>Contact Details</h3>

						<ul>
							<li>
								<p>Phone Number</p>
								<a href="tel:{!! getSitePhone() !!}">{!! getSitePhone() !!}</a>
							</li>
							<li>
								<p>Partner Enquiries</p>
								<a href="tel:{!! getPartnerEmail() !!}">{!! getPartnerEmail() !!}</a>
							</li>
							<li>
								<p>General Enquiries</p>
								<a href="tel:{!! getGeneralEmail() !!}">{!! getGeneralEmail() !!}</a>
							</li>
						</ul>

					</div>


				</div>

			</div>

		</div>

	@endwhile

@endsection
