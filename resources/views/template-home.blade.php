{{--
  Template Name: Home
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp

		@include('partials.home-intro')

		@include('partials.logo-slider')

		@include('partials.all-locations')

		@include('partials.about-storage-monkey')

		@include('partials.banner-space')

		@include('partials.latest-blog-posts')

	@endwhile

@endsection
