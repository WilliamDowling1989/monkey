{{--
  Template Name: Blog
--}}

@extends('layouts.app')

@section('content')

	@while(have_posts()) @php the_post() @endphp

		@include('partials.page-header')

		<div class="blog-page">
			
			<div class="blog-page__categories">
				
				<div class="container">

					<div class="blog-page__categories--inner">

						<ul>
							<li>
								<a href="{{ home_url('/') }}blog" <?php if(is_page('blogs-guides' ) ) { ?>  class="active" <?php } ?>>
									View All
								</a>
							</li>
						</ul>
						<ul>
							<?php wp_list_categories( array(
							    'orderby' => 'name',
							    'title_li' => '',
							) ); ?>
						</ul>

					</div>

				</div>

			</div>

			<div class="blog-page__posts">
				
				<div class="container">
					
					<div class="blog-page__posts--inner">

						@php($link = get_post_meta( get_the_ID(), 'advertising_space_link', true ) )
						@php($image = get_post_meta( get_the_ID(), 'advertising_space_image', true ) ) 

						<?php global $post; ?>
						@php($loop = getBlogPosts())

						@if( $loop->have_posts() )
						@php($count=1)
						  @while( $loop->have_posts() )
						    @php( $loop->the_post() )

						    		@if($count == 3)
									
									<a class="item" href="{!! $link !!}">
										<img src="{!! $image !!}" alt="">
									</a>
						    		
						    		@endif

								<div class="item">
									@include('partials.blog-card', [
									    'di_title' => get_the_title(),
									    'di_link'  => get_permalink(),
									     'di_category_name'  => get_the_category()
									])
								</div>

						  	@php($count++)
						  @endwhile
						@endif

						@php(wp_reset_postdata())

					</div>

				</div>

			</div>

			<div class="pagination">

				<div class="container">
					<?php if ($loop->max_num_pages > 1) : ?>
			   		 	<?php DiPagination($loop); ?>
					<?php endif; ?>
				</div>
				
			</div>

		</div>

	@endwhile

@endsection
