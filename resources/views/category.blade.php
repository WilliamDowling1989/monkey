@extends('layouts.app')

@section('content')

		<div class="blog-page">
			
			<div class="blog-page__categories">
				
				<div class="container">

					<div class="blog-page__categories--inner">

						<ul>
							<li>
								<a href="{{ home_url('/') }}blog" <?php if(is_page('blogs-guides' ) ) { ?>  class="active" <?php } ?>>
									View All
								</a>
							</li>
						</ul>
						<ul>
							<?php wp_list_categories( array(
							    'orderby' => 'name',
							    'title_li' => '',
							) ); ?>
						</ul>

					</div>

				</div>

			</div>

			<div class="blog-page__posts">
				
				<div class="container">
					
					<div class="blog-page__posts--inner">

						@while(have_posts()) @php(the_post())

							<div class="item">
								@include('partials.blog-card', [
								    'di_title' => get_the_title(),
								    'di_link'  => get_permalink(),
								     'di_category_name'  => get_the_category()
								])
							</div>

					  	@endwhile
    						@php(wp_reset_postdata())

					</div>

				</div>

			</div>

			<div class="pagination">

				<div class="container">

				</div>
				
			</div>

		</div>


@endsection
