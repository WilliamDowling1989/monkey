export default {
  init() {

    jQuery(document).ready(function($){
	    $(".menu-toggle").click(function() {
		    $(".nav-primary").toggleClass("nav-primary__show");
	    });
	 });

    $('.logo-slider__inner').slick({
        dots: true,
        infinite: true,
        speed: 300,
         arrows: false,
        slidesToShow: 5,
        slidesToScroll: 5,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [
         {
            breakpoint: 1200,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              dots: true
            }
          },

          {
            breakpoint: 500,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              dots: true
            }
          },
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });



    $('.latest-blog-posts__inner').slick({
        dots: true,
        infinite: false,
        speed: 300,
         arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
         {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 991,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true
            }
          },
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });



    $('.location-item .left .slider').slick({
        dots: true,
        infinite: true,
        arrows: false,
        speed: 300,
        slidesToShow: 1,
    });



     jQuery(document).ready(function($){

     	var $container = $('[data-size-illustrations]');
     	var $sizes =$container.data('size-illustrations');

     	$( "#field_2_2" ).append( "<div class='slider-text' data-size-text>Large Luton Van (75-100 sq ft)</div>" );

      $( "#field_2_2 .ginput_container" ).append( "<div class='dots'><ul><li></li><li></li><li></li><li></li><li></li><li></li><li></li><li></li></ul></div>");

     	$( "#field_2_2 .ginput_container" ).prepend( "<img class='slider-image' data-size-image src='/wp-content/themes/digitalimpact/dist/images/5.svg'>" );

     	$(document).on('change', '#input_2_2', function() {

     		var $val = $(this).val();

     		$('[data-size-text]').text($sizes[$val].text);
     		$('[data-size-image]').attr("src", $sizes[$val].image);
     	});

     });

   $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    });

       jQuery(document).ready(function($){
        new ModalVideo('.play-button', { 
          channel: 'youtube', 
          
        });
    });

    
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
